/*
 * CR95HF.c
 *
 *  Created on: Feb 7, 2018
 *      Author: Ethan Gibson
 */

#include "dCR95HF.h"

// todo: fix return values so that 0 is success and 1 is failed
// todo: expose all chip responses to the API
// todo: make even more modular to account more complex chip commands

int CR95HF_Read_Response(uint8_t spiDeviceNumber, uint8_t *data, uint8_t maxLength)
{
	int returnValue = 0;
	int result = 1;

	uint8_t txBuffer[MAX_SPI_FRAME] = {0};
	uint8_t recvHeaderBuffer[4] = {0};
	uint8_t txHeaderBuffer[3] = {0};

	txHeaderBuffer[0] = 0x2;
	txHeaderBuffer[1] = 0xFF;
	txHeaderBuffer[2] = 0xFF;

	for(int i = 0; i < 3; i++)
	{
		result = SPI_Data(spiDeviceNumber, txHeaderBuffer, 3, &recvHeaderBuffer[0]);
		if(result == ESP_OK)
		{
			if(recvHeaderBuffer[1] != 0xFF)
			{
				data[0] = recvHeaderBuffer[1];
				data[1] = recvHeaderBuffer[2];

				if(recvHeaderBuffer[2] && (recvHeaderBuffer[2] <= maxLength))
				{
					int dataLength = recvHeaderBuffer[2];

					for(int t = 0; t < dataLength; t++)
					{
						txBuffer[t] = 0xFF;
					}

					result = SPI_Data(spiDeviceNumber, txBuffer, dataLength, (data + 2));
					if(result == ESP_OK)
					{
						returnValue = dataLength;
						break;
					}
					else
					{
						returnValue = 0;
						break;
					}
				}
			}
		}
	}

	return returnValue;
}

int CR95HF_Read_Response2(uint8_t spiDeviceNumber, uint8_t *data, uint8_t maxLength)
{
	int result = 1;

    uint8_t txBuffer[MAX_SPI_FRAME] = {0};
    uint8_t recvHeaderBuffer[MAX_SPI_FRAME] = {0};
    txBuffer[0]=0x2;
    txBuffer[1]=0xFF;
    txBuffer[2]=0xFF;

    for(int t = 3; t < (maxLength + 3); t++)
    {
    	txBuffer[t] = 0xFF;
    }

    for(int i = 0; i < 3; i++)
    {
    	result = SPI_Data(spiDeviceNumber, txBuffer, (3+maxLength), &recvHeaderBuffer[0]);
    	if(result == ESP_OK)
    	{
    		if(recvHeaderBuffer[1] != 0xFF)
    		{
    			data[0] = recvHeaderBuffer[1];
    			data[1] = recvHeaderBuffer[2];

    			if(recvHeaderBuffer[2] && (recvHeaderBuffer[2] <= maxLength))
    			{
    				for(int j = 0; j < maxLength; j++)
    				{
    					data[j+2] = recvHeaderBuffer[j+3];
    				}

    				return recvHeaderBuffer[2];
    			}
    		}
    	}
    }
    return 0;
}

// Send the command to the CR95HF via SPI
void CR95HF_Send_Command(uint8_t spiDeviceNumber, uint8_t command, uint8_t length, uint8_t *data)
{
	uint8_t txBuffer[MAX_SPI_FRAME];

	txBuffer[0] = 0x00;
	txBuffer[1] = command;
	txBuffer[2] = length;

	printf("Send Command: ");
	for(int i = 0; i < 2; i ++)
	{
		printf("%02x", txBuffer[i]);
	}


	if(length > 0)
	{
		for(int i = 0; i < length; i++)
		{
			txBuffer[3 + i] = data[i];
			printf("%02x", data[i]);
		}
	}

	printf("\n");

	SPI_Command(spiDeviceNumber, &txBuffer[0], (3 + length));
}

// The CR95HF needs to be polled before commands can be sent
cr95hf_state CR95HF_Poll_Command(uint8_t spiDeviceNumber, uint8_t maxTries)
{
	int returnValue = CR95HF_Error;
	int result = 1;

	uint8_t recvBuffer[4] = {0};
	uint8_t txBuffer[2] = {0};

	txBuffer[0] = 0x03;
	txBuffer[1] = 0xff;

	for(int i = 0; i < maxTries; i++)
	{
		result = SPI_Data(spiDeviceNumber, txBuffer, 2, &recvBuffer[0]);

		if(result == ESP_OK)
		{
			if(recvBuffer[0] & 0x08)
			{
				returnValue = CR95HF_READABLE;
				break;
			}
		}
		else
		{
			vTaskDelay(100/portTICK_PERIOD_MS);
		}
	}

	return returnValue;
}

// CR95HF Commands
// 0x01 IDN - Requests short information about the CR95HF and its revision
uint8_t CR95HF_IDN(uint8_t spiDeviceNumber)
{
	uint8_t rxIDN[18] = {0};

	CR95HF_Send_Command(spiDeviceNumber, 0x01, 0x00, NULL);

	if(CR95HF_Poll_Command(spiDeviceNumber, 100) == CR95HF_READABLE)
	{
		CR95HF_Read_Response(spiDeviceNumber, &rxIDN[0], 18); //Possibly double check this command

		if(rxIDN[0] == 0x00)
			return 1;
	}

	return 0;
}

// 0x02 Protocol Select - Selcts the RF communication protocol and specifies protocol
// related parameters
// 0x02 <len> <Protocol> <Parameters>
uint8_t CR95HF_Protocol_Select(uint8_t spiDeviceNumber)
{
	uint8_t CR95HF_Protocol[2] =
	{
			CR95HF_PROTOCOL_15693,
			CR95HF_PROTOCOL_26KBPS |
			CR95HF_PROTOCOL_SOF |
			CR95HF_PROTOCOL_PWM10 |
			CR95HF_PROTOCOL_SUBCARRIER_S |
			CR95HF_PROTOCOL_CRC
	};

	// Send the Protocol Command
	CR95HF_Send_Command(spiDeviceNumber, 0x02, 2, &CR95HF_Protocol[0]);

	// Poll the chip and if it is successful continue
	if(CR95HF_Poll_Command(spiDeviceNumber, 100) == CR95HF_READABLE)
	{
		uint8_t rxProtocol[2] = {0};

		CR95HF_Read_Response(spiDeviceNumber, &rxProtocol[0], 2);

		if(rxProtocol[1] == 0x00)
		{
#ifdef DCR95HF_DEBUG
			ESP_LOGI(TAG_CR95HF, "Protocol select successful");
#endif
			return ESP_OK;
		}
	}
#ifdef DCR95HF_DEBUG
	ESP_LOGE(TAG_CR95HF, "Protocol select failed");
#endif
	return ESP_FAIL;
}

// 0x04 SendRecv - Sends data using the previously selected protocol and receives tag response
uint8_t CR95HF_SendRecv_Command(uint8_t spiDeviceNumber, uint8_t *tag)
{
	uint8_t CR95HF_Read_Tag[3] = {0x26, 0x01, 0x00};

	CR95HF_Send_Command(spiDeviceNumber, 0x04, 3, &CR95HF_Read_Tag[0]);

	if(CR95HF_Poll_Command(spiDeviceNumber, 100) == CR95HF_READABLE)
	{
		uint8_t rxReceive[15] = {0};

		CR95HF_Read_Response2(spiDeviceNumber, &rxReceive[0], sizeof(rxReceive));

#ifdef DCR95HF_DEBUG
		printf("Read Response: ");
#endif

		if((rxReceive[0] == 0x80)&&(rxReceive[1] == 0x0d)&&((rxReceive[14] & 0x03) == 0))
		{
			for(int i = 0; i < 8; i++)
			{
				tag[i] = rxReceive[4 + i];
#ifdef DCR95HF_DEBUG
				printf("%02x",rxReceive[i]);
#endif
			}
#ifdef DCR95HF_DEBUG
			printf("\n");
#endif
			return 1;
		}
	}

	return 0;
}

// 0x07 Idle - Switches CR95HF into a low consumption Wait for Event mode (Power-up, Hibernate,
// sleep or tag detection),  specifies the authorized wakeup sources and waits for an event to exit
// to Ready state

// todo: not the True Idle command since this only does one hardcoded thing. This should
// be modularized further to expose all chip Idle functionality
void CR95HF_Chip_Sleep(uint8_t spiDeviceNumber)
{
	static uint8_t hibernate[] = {0x08, 0x04, 0x00, 0x04, 0x00, 0x18, 0, 0, 0, 0, 0, 0, 0, 0};
	// Time to wait for bit to be set
	const TickType_t ticksToWait = 100/portTICK_PERIOD_MS;

	// Wait for the RFID Sleep event bit to be set
	EventBits_t sleepBits = xEventGroupWaitBits(
			sleepEventGroup,
			RFID_SLEEP,
			pdTRUE,
			pdFALSE,
			ticksToWait );

	if((sleepBits & (RFID_SLEEP)) == RFID_SLEEP)
	{
		CR95HF_Send_Command(spiDeviceNumber, 0x07, sizeof(hibernate), hibernate);
	}
	else
	{
#ifdef DCR95HF_DEBUG
		ESP_LOGE(TAG_CR95HF, "RFID sleep bit was never set. Function timed out.");
#endif
	}
}

// 0x08 RdReg - Reads wake-up event register or the Analog Register Configuration (ARC-B) register
// todo: implement this

// 0x09 WrReg - Writes Analog Register Configuration (ARC-B) register or writes index of ARC_B
// register address.
// Writes the Timer Window (TimerW) value dedicated to ISO/IEC 1443 Type A tags.
// Writes the AutoDetect Filter enabled register dedicated to ISO/IEC 18092 tags.
// todo: implement this

// 0x0A BaudRate - Sets the UART baud rate
// todo: implement this

// 0x55 Echo - CR95HF returns an Echo response
uint8_t CR95HF_Echo_Command(uint8_t spiDeviceNumber)
{
	CR95HF_Send_Command(spiDeviceNumber, 0x55, 0, NULL);

	if(CR95HF_Poll_Command(spiDeviceNumber, 100) == CR95HF_READABLE)
	{
		uint8_t rxEcho[2] = {0};

		if(rxEcho[0] == 0x05)
			return 1;
	}
#ifdef DCR95HF_DEBUG
	ESP_LOGI(TAG_CR95HF, "echo successful");
#endif
	return 0;
}

void RFID_Chip_Wakeup(void)
{
	gpio_set_level(RFID_IRQ_IN, 0);
	vTaskDelay(5/portTICK_PERIOD_MS);

	gpio_set_level(RFID_IRQ_IN, 1);
	vTaskDelay(20/portTICK_PERIOD_MS);
}

cr95hf_state CR95HF_Chip_Init(uint8_t spiDeviceNumber)
{
	int chipAwake = 1;
	int chipEcho = 1;
	int chipProtocol = 1;

	// todo: add in counter... don't do this forever
	do
	{
		if(chipAwake != 0)
		{
			RFID_Chip_Wakeup();
			chipAwake = 0;
		}

		if(chipEcho != 0)
		{
			chipEcho = CR95HF_Echo_Command(spiDeviceNumber);
		}

		if(chipProtocol != 0)
		{
			chipProtocol = CR95HF_Protocol_Select(spiDeviceNumber);
		}
		vTaskDelay(100 / portTICK_PERIOD_MS);

	}while((chipAwake & chipEcho & chipProtocol) == 1);

	return CR95HF_OK;
}


void CR95HF_Setup(void)
{
	// Time to wait for bit to be set
	const TickType_t ticksToWait = 500/portTICK_PERIOD_MS;

	SPI_Bus_Init();

	Init_SPI_Device(1, RFID_CS);

	// Wait for the RFID Setup event bit to be set
	EventBits_t setupBit = xEventGroupWaitBits(
			setupEventGroup,
			RFID_SETUP,
			pdTRUE,
			pdFALSE,
			ticksToWait );

	if((setupBit & RFID_SETUP) == RFID_SETUP)
	{
		if(CR95HF_Chip_Init(1) == CR95HF_OK)
		{
#ifdef DCR95HF_DEBUG
			ESP_LOGI(TAG_CR95HF, "Chip setup successful");
#endif
			xEventGroupSetBits(stateEventGroup, RFID_START);
			return;
		}
		else
		{
#ifdef DCR95HF_DEBUG
			ESP_LOGE(TAG_CR95HF, "CR95HF never initialized");
#endif
			return;
		}
	}
	else
	{
#ifdef DCR95HF_DEBUG
		ESP_LOGE(TAG_CR95HF, "RFID setup bit was never set. Function timed out.");
#endif
	}
}
