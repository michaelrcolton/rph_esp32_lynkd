/*
 * CR95HF.h
 *
 *  Created on: Feb 7, 2018
 *      Author: Ethan Gibson
 */

#ifndef MAIN_DCR95HF_H_
#define MAIN_DCR95HF_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "esp_log.h"
#include "driver/spi_master.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"

#include "eGlobal.h"
#include "Pin_Config.h"
#include "SPI_Helper.h"

#define TAG_CR95HF "CR95HF"

// Comment out to disable debug output
//#define DCR95HF_DEBUG

typedef enum cr95hf_state_
{
    CR95HF_Error   = -1,
    CR95HF_OK      = 0,
    CR95HF_READABLE= 1
}cr95hf_state;

enum CR95HF_PROTOCOL_SELECT
{
    CR95HF_PROTOCOL_FIELD_OFF   =0x00,
    CR95HF_PROTOCOL_15693       =0x01,
    CR95HF_PROTOCOL_14443A      =0x02,
    CR95HF_PROTOCOL_14443B      =0x03,
    CR95HF_PROTOCOL_18092       =0x04,

    CR95HF_PROTOCOL_26KBPS      =0x00,
    CR95HF_PROTOCOL_52KBPS      =0x10,
    CR95HF_PROTOCOL_06KBPS      =0x20,
    CR95HF_PROTOCOL_RFU         =0x30,

    CR95HF_PROTOCOL_312US       =0x00,
    CR95HF_PROTOCOL_SOF         =0x08,

    CR95HF_PROTOCOL_PWM100      =0x00,
    CR95HF_PROTOCOL_PWM10       =0x04,

    CR95HF_PROTOCOL_SUBCARRIER_S=0x00,
    CR95HF_PROTOCOL_SUBCARRIER_D=0x02,

    CR95HF_PROTOCOL_CRC         =0x01
};

int CR95HF_Read_Response(uint8_t spiDeviceNumber, uint8_t *data, uint8_t maxLength);
int CR95HF_Read_Response2(uint8_t spiDeviceNumber, uint8_t *data, uint8_t maxLength);

void CR95HF_Send_Command(uint8_t spiDeviceNumber, uint8_t command, uint8_t length, uint8_t *data);

cr95hf_state CR95HF_Poll_Command(uint8_t spiDeviceNumber, uint8_t maxTries);

uint8_t CR95HF_IDN(uint8_t spiDeviceNumber);
uint8_t CR95HF_Protocol_Select(uint8_t spiDeviceNumber);
uint8_t CR95HF_SendRecv_Command(uint8_t spiDeviceNumber, uint8_t *tag);
void CR95HF_Chip_Sleep(uint8_t spiDeviceNumber);
uint8_t CR95HF_Echo_Command(uint8_t spiDeviceNumber);

void RFID_Chip_Wakeup(void);

int CR95HF_Chip_Init(uint8_t spiDeviceNumber);

void CR95HF_Setup(void);

#endif /* MAIN_DCR95HF_H_ */
