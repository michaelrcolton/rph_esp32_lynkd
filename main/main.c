#include "main.h"

//Entry point for the state machine
void app_main(void)
{
	ESP_LOGI(TAG_MAIN,"Start App");
	Print_Product_FW_Version();

	// Setup Event Groups
	Create_Setup_Event_Group();
	Create_State_Event_Group();
	Create_Sleep_Event_Group();

	// Setup RFID Reader
	xEventGroupSetBits(setupEventGroup, RFID_SETUP);

	xTaskCreate(&RFID_Poll_Task, "RFID_Poll_Task", 4096, NULL, 10, &RFID_Poll_Task_Handle);
}
