/*
 * RFID_CR95HF.c
 *
 *  Created on: Feb 6, 2018
 *      Author: Ethan Gibson
 */

#include "iRFID.h"

TaskHandle_t RFID_Poll_Task_Handle = NULL;

rfid_return_type Poll_RFID()
{
	uint8_t tagRead[8] = {0};
	uint8_t result;

	result = CR95HF_SendRecv_Command(1, tagRead);
#ifdef IRFID_DEBUG
	ESP_LOGE(TAG_RFID, "SendRecv Result: %d", result);
#endif
	if(result)
	{
#ifdef IRFID_DEBUG
		ESP_LOGI(TAG_RFID, "Tag Detected: %02X,%02X,%02X,%02X,%02X,%02X,%02X,%02X\n", tagRead[0], tagRead[1], tagRead[2], tagRead[3], tagRead[4], tagRead[5], tagRead[6], tagRead[7]);
#endif



	}
	return RFID_Not_Matched;
}

void RFID_Poll_Task(void *pvParameters)
{
	//Setup CR95HF
	CR95HF_Setup();

	// Time to wait for bit to be set
	const TickType_t ticksToWait = pdMS_TO_TICKS(400);
#ifdef IRFID_DEBUG
	ESP_LOGI(TAG_RFID, "Task started");
#endif
	// Wait for the RFID start event bit to be set
	EventBits_t rfidBit = xEventGroupWaitBits(
			stateEventGroup,
			RFID_START,
			pdTRUE,
			pdFALSE,
			ticksToWait );

	if(((rfidBit & RFID_START) == RFID_START))
	{
#ifdef IRFID_DEBUG
		ESP_LOGI(TAG_RFID, "RFID Poll Start");
#endif
		xEventGroupSetBits(stateEventGroup, RFID_POLL);
		while(Poll_RFID() != RFID_Matched)
		{
#ifdef IRFID_DEBUG
			printf("Poll RFID");
#endif
			vTaskDelay(pdMS_TO_TICKS(200));
		}
		xEventGroupSetBits(stateEventGroup, RFID_POLL);
	}
	else
	{
#ifdef IRFID_DEBUG
		ESP_LOGE(TAG_RFID, "RFID sleep bit was never set. Function timed out.");
#endif
	}
#ifdef IRFID_DEBUG
	ESP_LOGE(TAG_RFID, "RFID Poll Exiting");
#ifdef IRFID_DEBUG
	RFID_Poll_Task_Handle = NULL;
	vTaskDelete(NULL);
}
