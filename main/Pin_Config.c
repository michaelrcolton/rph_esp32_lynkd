/*
 * Pin_Config.c
 *
 *  Created on: Feb 6, 2018
 *      Author: Ethan Gibson
 */

#include "Pin_Config.h"

void Accel_Pin_Init(void)
{}

void Accel_Pin_Deinit(void)
{}

void Button_Shield_Pin_Init(void)
{}

void Button_Shield_Pin_Deinit(void)
{}

void Buttons_Key_Pad_Pin_Init(void)
{}

void Buttons_Key_Pad_Pin_Deinit(void)
{}

void Button_Learning_Pin_Init(void)
{}

void Button_Learning_Pin_Deinit(void)
{}

void Flash_Pin_Init(void)
{}

void Flash_Pin_Deinit(void)
{}

void FP_Pin_Init(void)
{}

void FP_Pin_Deinit(void)
{}

void LED_White_Pin_Init(void)
{
	gpio_set_direction(LED_WHITE, GPIO_MODE_OUTPUT);
	gpio_set_level(LED_WHITE, 1);
}

void LED_White_Pin_Deinit(void)
{
	gpio_set_level(LED_WHITE, 1);
	gpio_set_direction(LED_WHITE, GPIO_MODE_INPUT);
	gpio_set_pull_mode(LED_WHITE, GPIO_FLOATING);
}

void RFID_Pin_Init(void)
{
	gpio_config_t rfid_irq_conf =
	{
		.intr_type = GPIO_PIN_INTR_DISABLE,
		.mode = GPIO_MODE_OUTPUT,
		.pin_bit_mask= BIT_MASK_PIN(RFID_IRQ_IN),
		.pull_down_en = 0,
		.pull_up_en = 1,
	};
	gpio_config(&rfid_irq_conf);

	gpio_config_t rfid_cs_conf =
	{
			.intr_type = GPIO_PIN_INTR_POSEDGE,
			.mode = GPIO_MODE_INPUT,
			// Why the learning button pin??
			.pin_bit_mask = BIT_MASK_PIN(BUTTON_LEARNING),
			.pull_down_en = 1,
			.pull_up_en = 0,
	};
	gpio_config(&rfid_cs_conf);
}

void RFID_Pin_Deinit(void)
{
	gpio_config_t rfid_irq_conf =
	{
			.intr_type = GPIO_PIN_INTR_DISABLE,
			.mode = GPIO_MODE_INPUT,
			.pin_bit_mask = BIT_MASK_PIN(RFID_IRQ_IN),
			.pull_down_en = 0,
			.pull_up_en = 1,
	};
	gpio_config(&rfid_irq_conf);

	gpio_config_t rfid_cs_conf =
	{
			.intr_type = GPIO_PIN_INTR_DISABLE,
			.mode = GPIO_MODE_INPUT,
			.pin_bit_mask = BIT_MASK_PIN(RFID_CS),
			.pull_down_en = 0,
			.pull_up_en = 1,
	};
	gpio_config(&rfid_cs_conf);

	gpio_set_level(RFID_IRQ_IN, 1);
	gpio_set_level(RFID_CS, 1);
}

void RGB_LED_PWM_Pin_Init(void)
{}

void RGB_LED_PWM_Pin_Deinit(void)
{}

void RGB_LED_GPIO_Pin_Init(void)
{
	gpio_set_direction(RGB_LED_RED, GPIO_MODE_OUTPUT);
	gpio_set_level(RGB_LED_RED, 1);

	gpio_set_direction(RGB_LED_GREEN, GPIO_MODE_OUTPUT);
	gpio_set_level(RGB_LED_GREEN, 1);

	gpio_set_direction(RGB_LED_BLUE, GPIO_MODE_OUTPUT);
	gpio_set_level(RGB_LED_BLUE, 1);
}

void RGB_LED_GPIO_Pin_Deinit(void)
{
	gpio_set_level(RGB_LED_RED, 1);
	gpio_set_direction(RGB_LED_RED, GPIO_MODE_INPUT);
	gpio_set_pull_mode(RGB_LED_RED, GPIO_FLOATING);

	gpio_set_level(RGB_LED_GREEN, 1);
	gpio_set_direction(RGB_LED_GREEN, GPIO_MODE_INPUT);
	gpio_set_pull_mode(RGB_LED_GREEN, GPIO_FLOATING);

	gpio_set_level(RGB_LED_BLUE, 1);
	gpio_set_direction(RGB_LED_BLUE, GPIO_MODE_INPUT);
	gpio_set_pull_mode(RGB_LED_BLUE, GPIO_FLOATING);
}

void Sensor_Touch_Init(void)
{}

void Sensor_Touch_Deinit(void)
{}

void Servo_PWM_Pin_Init(void)
{}

void Servo_PWM_Pin_Deinit(void)
{}

void Piezo_Buzzer_Pin_Init(void)
{}

void Piezo_Buzzer_Pin_Deinit(void)
{}

void Vbatt_Pin_Init(void)
{}

void Vbatt_Pin_Deinit(void)
{}

void Sleep_Pin_Config(void)
{
	// Turn off LEDs and set pins as floating inputs
	LED_White_Pin_Deinit();
	RGB_LED_GPIO_Pin_Deinit();

	SET_PERI_REG_MASK(RTC_CNTL_DIG_ISO_REG, RTC_CNTL_DG_PAD_AUTOHOLD_EN);
}

