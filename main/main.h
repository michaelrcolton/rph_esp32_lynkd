/*
 * main.h
 *
 *  Created on: Feb 6, 2018
 *      Author: Ethan Gibson
 */

#ifndef MAIN_MAIN_H_
#define MAIN_MAIN_H_

#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"

#include "dCR95HF.h"
#include "eGlobal.h"
#include "iRFID.h"

#define TAG_MAIN "Main App"

#endif /* MAIN_MAIN_H_ */
