/*
 * SPI_Helper.h
 *
 *  Created on: Feb 7, 2018
 *      Author: Ethan Gibson
 */

#ifndef MAIN_SPI_HELPER_H_
#define MAIN_SPI_HELPER_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "esp_err.h"
#include "esp_log.h"
#include "driver/spi_master.h"

#include "Pin_Config.h"

#define TAG_SPI "SPI Helper"
#define MAX_SPI_FRAME 64

extern spi_device_handle_t spiDevice1;
extern spi_device_handle_t spiDevice2;

esp_err_t SPI_Command(uint8_t spiDeviceNumber, const uint8_t *cmd, int len);
esp_err_t SPI_Data(uint8_t spiDeviceNumber, const uint8_t *data, int len, uint8_t *recv);

esp_err_t Init_SPI_Device(uint8_t spiDeviceNumber, uint8_t csPin);
esp_err_t SPI_Bus_Init();

#endif /* MAIN_SPI_HELPER_H_ */
