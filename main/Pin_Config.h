/*
 * Pin_Config.h
 *
 *  Created on: Feb 6, 2018
 *      Author: Ethan Gibson
 */

#ifndef MAIN_PIN_CONFIG_H_
#define MAIN_PIN_CONFIG_H_

#include "driver/gpio.h"
#include "driver/ledc.h"

#include "soc/gpio_struct.h"
#include "soc/rtc_cntl_reg.h"

#define BIT_MASK_PIN(x)	(1ULL<<x)

// Pin shared with SERVO_PWM
#define ACCEL_INT_1		4
#define ACC_PWR_CTRL	0	// Accessory Power Control: Power to Servo and FP Reader
#define ACC_CS			5	// Must be low when measuring battery

#define BUTTON_SHIELD	36

#define BUTTON_KEY_PAD_1	2
#define BUTTON_KEY_PAD_2	14
#define BUTTON_KEY_PAD_3	27
#define BUTTON_KEY_PAD_4	15
#define BUTTON_KEY_PAD_5	39

#define BUTTON_LEARNING	25

#define FLASH_HOLD		9
#define FLASH_WP		10
#define FLASH_CS		11
#define FLASH_SCK		6
#define FLASH_SDO		7
#define FLASH_SDI		8

#define FP_AWAKE		38
#define FP_RX			16	// RX UART line from fingerprint reader
#define FP_TX			17	// TX UART line to fingerprint reader

#define LED_WHITE		21

#define RFID_IRQ_IN		26
#define RFID_CS			12

#define RGB_LED_BLUE	22
#define RGB_LED_GREEN	3	// Pin shared with the Programming UART RX line
#define RGB_LED_RED		1	// Pin shared with the Programming UART TX line

#define SENSOR_TOUCH	37

// Pin shared with ACCEL_INT_1
#define SERVO_PWM		4

#define SPI_CLK			18
#define SPI_MOSI		23
#define SPI_MISO		19

#define PIEZO_BUZZER	13

#define VBATT_MEASURE	35

void Accel_Pin_Init(void);
void Accel_Pin_Deinit(void);

void Button_Shield_Pin_Init(void);
void Button_Shield_Pin_Deinit(void);

void Buttons_Key_Pad_Pin_Init(void);
void Buttons_Key_Pad_Pin_Deinit(void);

void Button_Learning_Pin_Init(void);
void Button_Learning_Pin_Deinit(void);

void Flash_Pin_Init(void);
void Flash_Pin_Deinit(void);

void FP_Pin_Init(void);
void FP_Pin_Deinit(void);

void LED_White_Pin_Init(void);
void LED_White_Pin_Deinit(void);

void RFID_Pin_Init(void);
void RFID_Pin_Deinit(void);

void RGB_LED_PWM_Pin_Init(void);
void RGB_LED_PWM_Pin_Deinit(void);
void RGB_LED_GPIO_Pin_Init(void);
void RGB_LED_GPIO_Pin_Deinit(void);

void Sensor_Touch_Init(void);
void Sensor_Touch_Deinit(void);

void Servo_PWM_Pin_Init(void);
void Servo_PWM_Pin_Deinit(void);

void Piezo_Buzzer_Pin_Init(void);
void Piezo_Buzzer_Pin_Deinit(void);

void Vbatt_Pin_Init(void);
void Vbatt_Pin_Deinit(void);

void Sleep_Pin_Config(void);

#endif /* MAIN_PIN_CONFIG_H_ */
