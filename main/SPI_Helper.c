/*
 * SPI_Helper.c
 *
 *  Created on: Feb 7, 2018
 *      Author: Ethan Gibson
 */

#include "SPI_Helper.h"

static SemaphoreHandle_t xSpiAccessSemaphore = NULL;
static unsigned int spiAccessMutexInitialized = 0;

spi_device_handle_t spiDevice1 = NULL;
spi_device_handle_t spiDevice2 = NULL;

void Init_SPI_Access_Mutex(void)
{
	if(spiAccessMutexInitialized == 0)
	{
		spiAccessMutexInitialized = 1;

		// Look into this
		xSpiAccessSemaphore = xSemaphoreCreateRecursiveMutex();

		if(xSpiAccessSemaphore == NULL)
		{
			ESP_LOGE(TAG_SPI, "SPI mutex not created");

			// What is this actually doing? Maybe looping until it triggers the Watchdog, then it resets
			while(1);
		}

		ESP_LOGI(TAG_SPI, "SPI mutex initialized");
	}
}

void Enter_SPI_Access_Mutex(void)
{
	if(spiAccessMutexInitialized == 1)
	{
		xSemaphoreTakeRecursive(xSpiAccessSemaphore, portMAX_DELAY);
		ESP_LOGI(TAG_SPI, "SPI mutex entered");
	}
	else
	{
		ESP_LOGE(TAG_SPI, "SPI cannot be accessed before initialized");
	}
}

void Exit_SPI_Access_Mutex(void)
{
	if(spiAccessMutexInitialized == 1)
	{
		xSemaphoreGiveRecursive(xSpiAccessSemaphore);
		ESP_LOGI(TAG_SPI, "SPI mutex exited");
	}
	else
	{
		ESP_LOGE(TAG_SPI, "SPI cannot be exited because it does not exist");
	}
}

esp_err_t SPI_Command(uint8_t spiDeviceNumber, const uint8_t *cmd, int len)
{
	esp_err_t ret;
	spi_transaction_t t;

	if(len == 0)
		return ESP_FAIL;

	Enter_SPI_Access_Mutex();

	memset(&t, 0, sizeof(t));

	t.length = len * 8;
	t.tx_buffer = cmd;

	if((spiDeviceNumber == 1) && (spiDevice1 != NULL))
	{
		ret = spi_device_transmit(spiDevice1, &t);
	}
	else if((spiDeviceNumber == 2) && (spiDevice2 != NULL))
	{
		ret = spi_device_transmit(spiDevice2, &t);
	}
	else
	{
		ESP_LOGE(TAG_SPI, "Invalid spi device number");
		return ESP_FAIL;
	}
	assert(ret == ESP_OK);

	Exit_SPI_Access_Mutex();

	ESP_LOGI(TAG_SPI, "SPI command sent");

	return ret;
}

esp_err_t SPI_Data(uint8_t spiDeviceNumber, const uint8_t *data, int len, uint8_t *recv)
{
	esp_err_t ret;
	spi_transaction_t t;

	if(len == 0)
		return ESP_FAIL;

	Enter_SPI_Access_Mutex();

	memset(&t, 0, sizeof(t));
	t.length = len * 8;
	t.tx_buffer = data;
	t.rx_buffer = recv;

	if((spiDeviceNumber == 1) && (spiDevice1 != NULL))
	{
		ret = spi_device_transmit(spiDevice1, &t);
	}
	else if((spiDeviceNumber == 2) && (spiDevice2 != NULL))
	{
		ret = spi_device_transmit(spiDevice2, &t);
	}
	else
	{
		ESP_LOGE(TAG_SPI, "Invalid spi device number");
		return ESP_FAIL;
	}
	assert(ret == ESP_OK);

	Exit_SPI_Access_Mutex();

	ESP_LOGI(TAG_SPI, "SPI data sent");

	return ret;
}

esp_err_t Init_SPI_Device(uint8_t spiDeviceNumber, uint8_t csPin)
{
	esp_err_t result;

	spi_device_interface_config_t devcfg1 =
	{
			.clock_speed_hz = 2000000,
			.mode = 0,
			.queue_size = 7,
			.spics_io_num = csPin,
	};

	if(spiDeviceNumber == 1)
	{
		result = spi_bus_add_device(HSPI_HOST, &devcfg1, &spiDevice1);
	}
	else if(spiDeviceNumber == 2)
	{
		result = spi_bus_add_device(HSPI_HOST, &devcfg1, &spiDevice2);
	}
	else
	{
		ESP_LOGE(TAG_SPI, "Invalid device number");
		return ESP_FAIL;
	}
	assert(result == ESP_OK);

	ESP_LOGI(TAG_SPI, "Add device OK: %d", result);

	return ESP_OK;
}

esp_err_t SPI_Bus_Init(void)
{
	esp_err_t result;

	spi_bus_config_t busConfig =
	{
			.miso_io_num = SPI_MISO,
			.mosi_io_num = SPI_MOSI,
			.sclk_io_num = SPI_CLK,
			.quadwp_io_num = -1,
			.quadhd_io_num = -1,
			.max_transfer_sz = 0,
	};

	Init_SPI_Access_Mutex();

//	for(int i = 0; i < 10; i++)
//	{
		result = spi_bus_initialize(HSPI_HOST, &busConfig, 0);
		assert(result == ESP_OK);
//	}

	ESP_LOGI(TAG_SPI, "SPI bus initialized");

	spiDevice1 = NULL;
	spiDevice2 = NULL;

	return ESP_OK;
}
