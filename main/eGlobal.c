/*
 * Global.c
 *
 *  Created on: Feb 6, 2018
 *      Author: Ethan Gibson
 */

#include "eGlobal.h"

void Create_Setup_Event_Group(void)
{
	setupEventGroup = xEventGroupCreate();

	if(setupEventGroup == NULL)
	{
		ESP_LOGE(TAG_GLOBAL, "Creating setup event group failed.");
	}
}

void Create_State_Event_Group(void)
{
	stateEventGroup = xEventGroupCreate();

	if(stateEventGroup == NULL)
	{
		ESP_LOGE(TAG_GLOBAL, "Creating state event group failed");
	}
}

void Create_Sleep_Event_Group(void)
{
	sleepEventGroup = xEventGroupCreate();

	if(sleepEventGroup == NULL)
	{
		ESP_LOGE(TAG_GLOBAL, "Creating sleep event group failed");
	}
}

void Print_Product_FW_Version(void)
{
	printf("%s%06x_%04x\n", PRODUCT_PREFIX, PRODUCT_ID, PRODUCT_FW_VERSION);
}
