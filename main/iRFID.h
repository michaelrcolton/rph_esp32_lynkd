/*
 * RFID_CR95HF.h
 *
 *  Created on: Feb 6, 2018
 *      Author: Ethan Gibson
 */

#ifndef MAIN_RFID_H_
#define MAIN_RFID_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "dCR95HF.h"
#include "esp_log.h"
#include "driver/spi_master.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"

#include "eGlobal.h"
#include "Pin_Config.h"
#include "SPI_Helper.h"

#define TAG_RFID "RFID"

// Comment out to disable debug output
#define IRFID_DEBUG

#define MAX_TAGS 100

typedef enum
{
		RFID_Matched = 0,
		RFID_Not_Matched = 1,
		RFID_Learned = 2,
		RFID_Not_Learned = 3,
} rfid_return_type;

extern TaskHandle_t RFID_Poll_Task_Handle;

void RFID_Poll_Task(void *pvParameters);

#endif /* MAIN_RFID_H_ */
