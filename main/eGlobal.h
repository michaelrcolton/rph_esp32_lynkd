/*
 * Global.h
 *
 *  Created on: Feb 6, 2018
 *      Author: Ethan Gibson
 */

#ifndef MAIN_GLOBAL_H_
#define MAIN_GLOBAL_H_

#include <stdio.h>
#include <stdlib.h>

#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "soc/soc.h"

#define TAG_GLOBAL "Global"

#define PRODUCT_PREFIX		"R"
#define PRODUCT_ID			0x003541
#define PRODUCT_FW_VERSION	0x00BC

// State Event Group Bits
#define RFID_START		BIT0
#define RFID_POLL		BIT1
#define RFID_LEARN		BIT2
#define FP_START		BIT3
#define FP_POLL			BIT4
#define FP_LEARN		BIT5
#define KEY_PAD_START	BIT6
#define	KEY_PAD_POLL	BIT7
#define KEY_PAD_LEARN	BIT8
#define BLUETTOTH_START	BIT9
#define BLUETOOTH_ON	BIT10
#define WIFI_START		BIT11
#define WIFI_ON			BIT12

// Setup Event Group Bits
#define RFID_SETUP		BIT0
#define FP_SETUP		BIT1
#define	KEY_PAD_SETUP	BIT2
#define BLUETOOTH_SETUP	BIT3
#define WIFI_SETUP		BIT4

// Sleep Event Group Bits
#define RFID_SLEEP		BIT0
#define FP_SLEEP		BIT1
#define	KEY_PAD_SLEEP	BIT2
#define BLUETOOTH_SLEEP	BIT3
#define WIFI_SLEEP		BIT4

EventGroupHandle_t stateEventGroup;
EventGroupHandle_t setupEventGroup;
EventGroupHandle_t sleepEventGroup;

void Create_Setup_Event_Group(void);
void Create_State_Event_Group(void);
void Create_Sleep_Event_Group(void);

void Print_Product_FW_Version(void);

#endif /* MAIN_GLOBAL_H_ */
